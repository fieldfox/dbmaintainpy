__author__ = 'wim'

import os
import math
import hashlib


class SqlScript:
    def __init__(self, root, filename):
        hasher = hashlib.md5()
        self.filename = filename
        self.location = os.path.join(root, filename)
        self.content = ""

        if os.path.isfile(self.location):
            with open(self.location, 'rb') as fo:
                self.content = fo.read()
                hasher.update(self.content)

        self.checksum = hasher.hexdigest()
        self.lastModificationDate = math.floor(os.path.getmtime(self.location))

    def execute(self, connection):
        scriptCur = connection.cursor()
        scriptCur.executescript(self.content)


