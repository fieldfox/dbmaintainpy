import datetime
import math
from Dbmaintain import Dbmaintain

__author__ = 'wim'

import sys
import sqlite3

from optparse import OptionParser

def main():
    parser = OptionParser()
    parser.add_option("--scripts",
                      help="Location of the database scripts",
                      dest="scriptsLocation")
    parser.add_option("--db",
                      help="Location of the database",
                      dest="database")
    parser.add_option("--continueOnError",
                      help="Continue on error",
                      dest="continueOnError",
                      type="int")
    parser.add_option("--fromScratch",
                      help="Create database from Scratch",
                      dest="fromScratch",
                      type="int")
    parser.set_defaults(
        scriptsLocation='C:\Users\wim\Source\QBMT\ZOModules\databasescripts',
        database="myDb.db",
        continueOnError=0,
        fromScratch=0)

    (opts, args_) = parser.parse_args()

    scriptsLocation = opts.scriptsLocation
    continueOnError = opts.continueOnError
    fromScratch = opts.fromScratch
    database = opts.database

    conn = sqlite3.connect(database);

    dbmaintain = Dbmaintain(conn, scriptsLocation, fromScratch)
    dbmaintain.initialise()
    scripts = dbmaintain.loadScriptFiles()
    executedScripts = dbmaintain.loadExecutedScripts()

    for i, script in enumerate(scripts):
            #if(executedScripts)
            if executedScripts.__len__() > i:
                if script.filename == executedScripts[i].filename and script.checksum == executedScripts[i].checksum:
                    print "same files"
                    continue
                else:
                    print "File changed"
                    if(not continueOnError):
                        break
            try:
                print "Executing {0} ".format(script.filename)
                script.execute(conn)
                dbmaintain.logExecution(script, 1)

            except Exception:
                print "Failed to execute {0}".format(script.filename)
                dbmaintain.logExecution(script, 0)
                if(not continueOnError):
                    break
            finally:
                conn.commit()

    conn.close()

    sys.exit(0)


if __name__ == "__main__":
    main()
