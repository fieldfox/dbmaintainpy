__author__ = 'wim'

import sys
import os
import datetime
from SqlScript import SqlScript

class Dbmaintain:
    def __init__(self, connection, scriptLocation, fromScratch=False):
        self.scriptLocation = scriptLocation
        self.connection = connection
        self.createTableScript = """
        CREATE TABLE 'dbmaintain_scripts' (
        'filename'  TEXT NOT NULL,
        'file_last_modified_at'  INTEGER NOT NULL,
        'checksum'  TEXT NOT NULL,
        'executed_at'  TEXT NOT NULL,
        'succeeded'  INTEGER NOT NULL DEFAULT 0 );
        """

        self.fromScratch = fromScratch
        self.executedScripts = []

        pass

    def createScriptTable(self):
        cur = self.connection.cursor()

        cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
        tables = cur.fetchall()

        if (u'dbmaintain_scripts',) not in tables:
            cur.executescript(self.createTableScript)
            return True
        else:
            if cur.execute("SELECT * FROM dbmaintain_scripts WHERE succeeded = 0").fetchall().__len__()>0 :
                print "Database is not up-to-date, cannot continue."
                sys.exit(1)
            return False

    def dropAllTables(self):
        cur = self.connection.cursor()

        cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
        tables = cur.fetchall()

        for table in tables:
            cur.execute("DROP TABLE " + table[0])


    def loadExecutedScripts(self):
        cur = self.connection.cursor()
        executed = cur.execute("SELECT * FROM dbmaintain_scripts").fetchall()
        for row in executed:
            sqlScript = SqlScript(self.scriptLocation, str(row[0]))
            sqlScript.lastModificationDate = row[1]
            sqlScript.checksum = str(row[2])
            sqlScript.executedAt = str(row[3])
            sqlScript.succeeded = row[4]
            self.executedScripts.append(sqlScript)
        return self.executedScripts

    def loadScriptFiles(self):
        scripts = []
        root = self.scriptLocation
        for dirName, subdirList, fileList in os.walk(root):
            for fname in fileList:
                fileName = os.path.relpath(os.path.join(root, dirName, fname), root)
                sqlScript = SqlScript(root, fileName)
                scripts.append(sqlScript)
        return scripts

    def logExecution(self, script, succeeded):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        cur = self.connection.cursor()
        cur.execute('INSERT INTO "dbmaintain_scripts" ("filename", "file_last_modified_at", "checksum", "executed_at", "succeeded") VALUES (?, ?, ?, ?, ?)',
                        (script.filename, script.lastModificationDate, script.checksum, now, succeeded))


    def initialise(self):
        if(self.fromScratch):
            self.dropAllTables()
            pass


        if not self.createScriptTable():
            self.loadExecutedScripts()


